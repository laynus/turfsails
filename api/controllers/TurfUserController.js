/**
 * TurfUserController
 *
 * @description :: Server-side logic for managing Turfusers
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
    index: function (req, res) {
      var request = require('request');

      var options = {
        url: 'http://api.turfgame.com/v4/users',
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        json: [{
          "name": ""
        }]
      };

      request(options, function (error, response, body) {
        if (!error && response.statusCode == 200) {
          res.send(response.body);
        }
      });

    }
};



